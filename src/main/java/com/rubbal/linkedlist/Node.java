package com.rubbal.linkedlist;

import lombok.Data;

import java.net.InetAddress;

@Data
public class Node {
    private InetAddress value;
    private Node prev;
    private Node next;

    public void remove() {
        if (prev != null) {
            prev.setNext(next);
        }
        if (next != null) {
            next.setPrev(prev);
        }
    }


    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }
}
