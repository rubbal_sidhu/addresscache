package com.rubbal.linkedlist;

import lombok.Data;

@Data
public class List {
    private Node head;

    public void pop() {
        this.head = head.getNext();
        if (head != null) {
            head.setPrev(null);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        Node n = head;
        while (n != null) {
            sb.append(n.getValue().toString() + ",");
            n = n.getNext();
        }
        return sb.toString();
    }
}
