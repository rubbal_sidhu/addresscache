package com.rubbal.addresscache;

import com.rubbal.linkedlist.List;
import com.rubbal.linkedlist.Node;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class AddressCache implements AddressCacheAPI {

    private ReadWriteLock lock;
    private List list;
    private Map<InetAddress, Node> map;
    private final Object monitor = new Object();

    public AddressCache(long maxAge, TimeUnit unit) {
        this.map = new HashMap<InetAddress, Node>();
        this.lock = new ReentrantReadWriteLock();

    }

    @Override
    public boolean add(InetAddress address) {
        if (map.containsKey(address)) {
            return false;
        }

        lock.writeLock().lock();

        Node node = new Node();
        node.setValue(address);

        if (list == null || list.getHead() == null) {
            list = new List();
            list.setHead(node);
            synchronized (monitor) {
                monitor.notify();
            }
        } else {
            node.setNext(this.list.getHead());
            this.list.getHead().setPrev(node);
            this.list.setHead(node);
        }
        map.put(address, node);

        lock.writeLock().unlock();
        return true;

    }

    @Override
    public boolean remove(InetAddress address) {
        if (!map.containsKey(address)) {
            return false;
        }

        lock.writeLock().lock();
        Node node = map.get(address);

        if (node.getPrev() == null) {
            list.pop();
        } else {
            node.remove();
        }
        map.remove(address);
        lock.readLock();
        return true;
    }

    @Override
    public InetAddress peek() {
        if (list == null || list.getHead() == null) {
            return null;
        }
        return list.getHead().getValue();
    }

    @Override
    public InetAddress take() {
        synchronized (monitor) {
            if (list == null || list.getHead() == null) {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            lock.writeLock().lock();
            InetAddress retValue = list.getHead().getValue();
            map.remove(retValue);
            list.pop();
            lock.writeLock().unlock();
            return retValue;

        }
    }

    @Override
    public String toString() {
        return "AddressCache{" +
                "list=" + list +
                '}';
    }
}
