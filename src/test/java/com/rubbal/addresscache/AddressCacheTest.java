package com.rubbal.addresscache;


import junit.framework.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class AddressCacheTest {
    private AddressCache addressCache;

    @BeforeTest
    public void setUp() {
    }

    @Test
    public void testAddition() throws UnknownHostException {
        addressCache = new AddressCache(10l, TimeUnit.HOURS);

        // Test adddition in Empty Cache.
        boolean status = addressCache.add(InetAddress.getByAddress("dummyName", new byte[]{1, 2, 3, 4}));
        Assert.assertEquals(true, status);
        System.out.println(addressCache);

        // Test addition if already exists.
        status = addressCache.add(InetAddress.getByAddress("dummyName", new byte[]{1, 2, 3, 4}));
        Assert.assertEquals(false, status);
        System.out.println(addressCache);

        // Test multiple Additions
        for (int i = 2; i < 6; i++) {
            status = addressCache.add(InetAddress.getByAddress("dummyName", new byte[]{(byte) i, 2, 3, 4}));
            Assert.assertEquals(true, status);
        }
        System.out.println(addressCache);
    }

    @Test
    public void testRemoval() throws UnknownHostException {
        addressCache = new AddressCache(10l, TimeUnit.HOURS);

        // Test single removal
        InetAddress addressToRemove = InetAddress.getByAddress("dummyName", new byte[]{1, 2, 3, 4});
        addressCache.add(addressToRemove);
        Assert.assertEquals(true, addressCache.remove(addressToRemove));
        System.out.println(addressCache);

        // Test removal when not exists
        Assert.assertEquals(false, addressCache.remove(addressToRemove));
        System.out.println(addressCache);

        // Test removal from multiple addresses
        for (int i = 2; i < 10; i++) {
            boolean status = addressCache.add(InetAddress.getByAddress("dummyName", new byte[]{(byte) i, 2, 3, 4}));
            Assert.assertEquals(true, status);
            InetAddress x = InetAddress.getByAddress("dummyName", new byte[]{(byte) i, 2, 3, 4});
        }

        Assert.assertEquals(true, addressCache.remove(InetAddress.getByAddress("dummyName", new byte[]{(byte) 2, 2, 3, 4})));
        Assert.assertEquals(true, addressCache.remove(InetAddress.getByAddress("dummyName", new byte[]{(byte) 3, 2, 3, 4})));
        Assert.assertEquals(true, addressCache.remove(InetAddress.getByAddress("dummyName", new byte[]{(byte) 8, 2, 3, 4})));
        Assert.assertEquals(true, addressCache.remove(InetAddress.getByAddress("dummyName", new byte[]{(byte) 9, 2, 3, 4})));

        System.out.println(addressCache);
    }

    @Test
    public void testPeek() throws UnknownHostException {
        addressCache = new AddressCache(10l, TimeUnit.DAYS);

        // Test peek when empty
        Assert.assertEquals(addressCache.peek(), null);

        for (int i = 2; i < 10; i++) {
            boolean status = addressCache.add(InetAddress.getByAddress("dummyName", new byte[]{(byte) i, 2, 3, 4}));
            Assert.assertEquals(true, status);
            InetAddress x = InetAddress.getByAddress("dummyName", new byte[]{(byte) i, 2, 3, 4});
        }

        // Test peek with multiple elements
        Assert.assertEquals(InetAddress.getByAddress("dummyName", new byte[]{(byte) 9, 2, 3, 4}), addressCache.peek());
        Assert.assertEquals(true, addressCache.remove(InetAddress.getByAddress("dummyName", new byte[]{(byte) 9, 2, 3, 4})));

        // Test peek after the most recent element deletion
        Assert.assertEquals(InetAddress.getByAddress("dummyName", new byte[]{(byte) 8, 2, 3, 4}), addressCache.peek());
        Assert.assertEquals(true, addressCache.remove(InetAddress.getByAddress("dummyName", new byte[]{(byte) 3, 2, 3, 4})));

        // Test peek after random deletion
        Assert.assertEquals(true, addressCache.remove(InetAddress.getByAddress("dummyName", new byte[]{(byte) 8, 2, 3, 4})));
    }

    @Test
    public void testTakeInNonEmptyCache() throws UnknownHostException {
        addressCache = new AddressCache(10l, TimeUnit.DAYS);

        for (int i = 2; i < 10; i++) {
            boolean status = addressCache.add(InetAddress.getByAddress("dummyName", new byte[]{(byte) i, 2, 3, 4}));
            Assert.assertEquals(true, status);
            InetAddress x = InetAddress.getByAddress("dummyName", new byte[]{(byte) i, 2, 3, 4});
        }

        // Test take with non empty cache
        Assert.assertEquals(InetAddress.getByAddress("dummyName", new byte[]{(byte) 9, 2, 3, 4}), addressCache.take());

        // Test peek again
        Assert.assertEquals(InetAddress.getByAddress("dummyName", new byte[]{(byte) 8, 2, 3, 4}), addressCache.take());

        // Test peek after deleting the next take element
        System.out.println(addressCache);
        Assert.assertEquals(true, addressCache.remove(InetAddress.getByAddress("dummyName", new byte[]{(byte) 7, 2, 3, 4})));
        System.out.println(addressCache);
        Assert.assertEquals(InetAddress.getByAddress("dummyName", new byte[]{(byte) 6, 2, 3, 4}), addressCache.take());
    }

    @Test
    public void testTakeInEmptyCache() throws UnknownHostException, InterruptedException {
        addressCache = new AddressCache(10l, TimeUnit.DAYS);

        // Test take with empty cache
        Thread take1 = new Thread(new TakeInvokingThread());
        take1.start();

        Thread.sleep(1);
        // Check if it is waiting
        Assert.assertEquals(Thread.State.WAITING, take1.getState());

        // Add an address
        addressCache.add(InetAddress.getByAddress("dummyName", new byte[]{(byte) 1, 2, 3, 4}));
        Thread.sleep(1);

        // See if the return value is correct
        Assert.assertEquals(InetAddress.getByAddress("dummyName", new byte[]{(byte) 1, 2, 3, 4}), returnValue);
        Assert.assertEquals(addressCache.peek(), null);

        // Test multiple threads blocked for a take

        Thread take2 = new Thread(new TakeInvokingThread());
        take2.start();

        Thread take3 = new Thread(new TakeInvokingThread());
        take3.start();

        Thread.sleep(2);
        Assert.assertEquals(Thread.State.WAITING, take2.getState());
        Assert.assertEquals(Thread.State.WAITING, take3.getState());

        // Add exactly one element
        addressCache.add(InetAddress.getByAddress("dummyName", new byte[]{(byte) 1, 2, 3, 4}));

        Thread.sleep(10);
        // Exactly one of the two threads must still be waiting
        int i = 0;
        if (take2.getState().equals(Thread.State.WAITING)) {
            i++;
        }

        if (take3.getState().equals(Thread.State.WAITING)) {
            i++;
        }

        Assert.assertEquals(1, i);

        // Add Another element
        addressCache.add(InetAddress.getByAddress("dummyName", new byte[]{(byte) 1, 2, 3, 4}));

        Thread.sleep(1);
        // Both threads should have terminated
        Assert.assertEquals(Thread.State.TERMINATED, take2.getState());
        Assert.assertEquals(Thread.State.TERMINATED, take3.getState());


    }

    private volatile InetAddress returnValue;

    public class TakeInvokingThread implements Runnable {

        @Override
        public void run() {
            returnValue = addressCache.take();
            System.out.println("Take operation returned: " + returnValue);
        }
    }

}
